package ExpRegulares;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

	private static Pattern pattern;
	private static Matcher matcher;
	public static void main(String[] args) 
	{
		checkClaveBanco();
		System.out.println(detectaNombresDeArchivos("archivo.l1l"));
		System.out.println(detectaNombresDeArchivos("archivo.l1l4"));
		System.out.println(detectaNombresDeArchivos("archiv o.txt"));
		System.out.println(detectaNombresDeArchivosSinExtension("archivol"));
		System.out.println(validateDate("32/4/2017"));
		
	}

	private static String validateDate(String date) 
	{
		//"\\d{1,2}/\\d{1,2}/\\d{4}"
		pattern = Pattern.compile("^(0?[1-9]|[12][0-9]|3[01])[\\/](0?[1-9]|1[012])[/\\/](19|20)\\d{2}$");
		matcher = pattern.matcher(date);
		String ret = ""; 
		if (matcher.matches())
		{
			ret = "La fecha '"+date+"' es válida!";
		}
		else
			ret = "La fecha '"+date+"' es invalida!";
		return ret;


	}

	private static String detectaNombresDeArchivosSinExtension(String archivo)
	{
		pattern = Pattern.compile("[a-z,A-Z,0-9-_]{1,8}");
		matcher = pattern.matcher(archivo);
		String ret = ""; 
		if (matcher.matches())
		{
			ret = "El nombre de archivo '"+archivo+"' es válido!";
		}
		else
			ret = "El nombre de archivo '"+archivo+"' esta mal escrito!";
		return ret;
	}

	private static String detectaNombresDeArchivos(String archivo) 
	{
		pattern = Pattern.compile("[a-z,A-Z,0-9-_]{1,8}\\.[a-z,A-Z,0-9-_]{1,3}");
		matcher = pattern.matcher(archivo);
		String ret = ""; 
		if (matcher.matches())
		{
			ret = "El nombre de archivo '"+archivo+"' es válido!";
		}
		else
			ret = "El nombre de archivo '"+archivo+"' esta mal escrito!";
		return ret;
		
	}

	private static void checkClaveBanco() 
	{
		//pattern = Pattern.compile("\\d\\d\\d\\d");
		pattern = Pattern.compile("\\d{4}");
		matcher = pattern.matcher("09448");
		if (matcher.matches())
		{
			System.out.println("Su clave es válida!");
		}
		else
			System.out.println("Clave erronea!");
	}
	
	
/*
 * 
 * System.out.println("Encontre " + matcher.group() + " comenzando en "
		+ matcher.start() + " y terminando en " + matcher.end());	
 */
}
